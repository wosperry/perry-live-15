﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Abc.Domain.Users;

public class User
{
    private User()
    {
        
    }

    public User(string account,string password,string salt ,string name,string mobile)
    {
        Account = account;
        Password = password;
        Salt = salt;
        Name = name;
        Mobile = mobile;
    }

    /// <summary>
    /// Password Sort
    /// </summary>
    public string Salt { get; set; }

    /// <summary>
    /// ID
    /// </summary>
    public long Id { get; set; }
    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; private set; }
    /// <summary>
    /// 用户名
    /// </summary>
    public string Account { get;  private set;}

    /// <summary>
    /// 密码
    /// </summary> 
    public string Password { get;  private set; }

    /// <summary>
    /// 电话号码
    /// </summary>
    private string _mobile;
    public string Mobile { get; private set; }
    
    /// <summary>
    /// 生日
    /// </summary>
    public DateTime Birthday { get; set; }
    /// <summary>
    /// 岁数
    /// </summary>
    [NotMapped]
    public int Age => DateTime.Today.Year - Birthday.Year;
 
}