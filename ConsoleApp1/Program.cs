﻿#pragma warning disable CS8321 
// (a, b) => a+b;

// Func<int, int, int> func = (a, b) => a + b; // lambda expression

// t=> t.Name.StartWith("张") && t.Age > 18
// collection.Where( t=> t.Name.StartWith("张") && t.Age > 18 )

using System.Linq.Expressions;

var paramA = Expression.Parameter(typeof(int), "a");
var paramB = Expression.Parameter(typeof(int), "b");

var returnExpression = Expression.Add(paramA, paramB);

var funcExpression = Expression.Lambda<Func<int, int, int>>(returnExpression, paramA, paramB);

Func<int, int, int> func = funcExpression.Compile();

var c = func(3, 23);

Console.WriteLine(funcExpression.ToString()); 
Console.WriteLine(c);