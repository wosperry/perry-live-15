﻿using Abc.Domain.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Abc.EFCore.EntityTypeConfigurations;

public class UserTypeConfiguration:IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.ToTable("sys_user");
        builder.HasKey(user => user.Id);
        builder.HasIndex(user => user.Account).IsUnique();
        
        builder.Property(user=>user.Id)
            .HasValueGenerator<SnowFlakeGenerator>()
            .HasColumnName("id").HasComment("ID");
        builder.Property(user=>user.Name).HasMaxLength(100).HasColumnName("name").HasComment("姓名");
        builder.Property(user=>user.Account).HasMaxLength(100).HasColumnName("account").HasComment("用户名");
        builder.Property(user=>user.Password).HasMaxLength(100).HasColumnName("password").HasComment("密码");
        builder.Property(user=>user.Salt).HasMaxLength(100).HasColumnName("salt").HasComment("盐");
        builder.Property(user=>user.Mobile).HasMaxLength(11).HasColumnName("mobile").HasComment("手机号码");
        builder.Property(user=>user.Birthday).HasColumnName("birthday").HasComment("生日");
    }
}