﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Abc.EFCore.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "sys_user",
                columns: table => new
                {
                    id = table.Column<long>(type: "INTEGER", nullable: false, comment: "ID")
                        .Annotation("Sqlite:Autoincrement", true),
                    salt = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false, comment: "盐"),
                    name = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false, comment: "姓名"),
                    account = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false, comment: "用户名"),
                    password = table.Column<string>(type: "TEXT", maxLength: 100, nullable: false, comment: "密码"),
                    mobile = table.Column<string>(type: "TEXT", maxLength: 11, nullable: false, comment: "手机号码"),
                    birthday = table.Column<DateTime>(type: "TEXT", nullable: false, comment: "生日")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_account",
                table: "sys_user",
                column: "account",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "sys_user");
        }
    }
}
