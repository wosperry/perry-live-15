﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using Yitter.IdGenerator;

namespace Abc.EFCore;

public class SnowFlakeGenerator : ValueGenerator<long>
{
    public override long Next(EntityEntry entry)
    {
        return YitIdHelper.NextId();
    }

    public override bool GeneratesTemporaryValues => true;
}