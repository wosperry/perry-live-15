﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace Abc.EFCore;

public class DefaultDbContext:DbContext 
{
    public DefaultDbContext(DbContextOptions<DefaultDbContext> options):base(options)
    {
            
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}