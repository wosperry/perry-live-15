import type { AppRouteModule } from '/@/router/types'; 
import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const system: AppRouteModule = {
  path: '/system',
  name: 'System',
  component: LAYOUT,
  meta: { 
    icon: 'iconoir:wrench',
    title:'系统管理',
    orderNo: 200000,
  },
  children: [
    {
      path: 'user',
      name: 'UserManagement',
      meta: {
        title: t('routes.system.userManagement.title'),
        icon: 'iconoir:user',
      },
      redirect: '/system/user/userinfo',
      children:[
        {
          path:'userinfo',
          name:'UserInfo',
          component: () => import('/@/views/system/user/index.vue'),
          meta:{
            title: t('routes.system.userManagement.userinfo'), 
            icon: 'iconoir:user', 
          },
        },
        {
          path:'userlist',
          name:'UserList',
          component: () => import('/@/views/system/user/list.vue'),
          meta:{
            title: t('routes.system.userManagement.userlist'), 
            icon: 'iconoir:user'
          }
        }
      ]
    },
  ],
};

export default system;
