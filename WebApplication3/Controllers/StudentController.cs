﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication3.EFCore;

namespace WebApplication3.Controllers
{
    [Route("student")]
    public class StudentController: ControllerBase
    {
        private readonly MyDbContext _context;

        public StudentController(MyDbContext context)
        { 
            _context = context;
        }
         

        [HttpPost]
        public async Task<Student> InsertAsync(StudentCreateDto input)
        {
            var student = new Student
            {
                Name= input.Name,
                NickName= input.NickName,
                Birthday=input.Birthday
            };
            await _context.Set<Student>().AddAsync(student);
            await _context.SaveChangesAsync();

            return student;
        }

        [HttpGet]
        public async Task<List<Student>> GetAllAsync()
        {
            return await _context.Set<Student>().ToListAsync();
        }

        [HttpDelete]
        public async Task DeleteAsync(long id)
        {
            var student = await _context.Set<Student>().FirstAsync(t=>t.Id == id);
             _context.Set<Student>().Remove(student);
            await _context.SaveChangesAsync();
        }
    }

    public class StudentCreateDto
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public DateTime Birthday { get; set; }
    }
}
