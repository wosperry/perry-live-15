using Microsoft.EntityFrameworkCore;
using MyEFCore;
using WebApplication3.EFCore;

var builder = WebApplication.CreateBuilder(args);
 

builder.Services.AddMyDbContext<MyDbContext>((options, connectionString) =>
{
    options.UseSqlServer(connectionString);
}) ; 

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerDocument();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseOpenApi();
    app.UseSwaggerUi3();
}

app.UseAuthorization();

app.MapControllers();

app.Run();

