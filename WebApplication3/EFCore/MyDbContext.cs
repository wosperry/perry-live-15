﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyEFCore;

namespace WebApplication3.EFCore
{
    [ConnectionStringName("SqlServer")]
    public class MyDbContext : BaseDbContext
    { 
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }


    public class Student: Entity<long>, ISoftDelete
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public DateTime Birthday { get; set; }
        public bool IsDeleted { get; set; } 
    }

    public class StudentEntityTypeConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable("student");

            builder.Property(x => x.Name).HasMaxLength(50).HasColumnName("name").HasComment("名字");
            builder.Property(x => x.NickName).HasMaxLength(50).HasColumnName("nickname").HasComment("昵称");
            builder.Property(x => x.Birthday).HasColumnName("birth").HasComment("生日");
        }
    }
}
