﻿
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc; 

namespace WebApplication2.Controllers
{

    [Route("hello")]
    public class HelloController:ControllerBase
    {
        public int MyProperty { get; set; }

        [HttpGet]
        public string Hello()
        {
            return "hello world";
        }
    }
}
