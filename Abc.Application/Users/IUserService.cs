﻿using Abc.Application.Dtos;

namespace Abc.Application.Users;

public interface IUserService
{
    public Task<UserDto> RegisterAsync(string account, string password, string name, string mobile,
        DateTime birthday);

    public Task<PagedResultDto<UserDto>> GetPagedListAsync(string? account, string? name, string? mobile,
        DateTime? birthday,
        int pageIndex = 1, int pageSize = 10);

    Task<bool> CheckLoginAsync(string username, string password);
    Task<UserInfoDto> GetUserInfo(string username);
}