﻿using Abc.Domain.Users;
using AutoMapper;

namespace Abc.Application.Users;

public class UserAutoMapperProfile:Profile
{
    public UserAutoMapperProfile()
    {
        CreateMap<User, UserDto>();
        CreateMap<User, UserInfoDto>();
    }
}