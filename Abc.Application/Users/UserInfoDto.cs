﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abc.Application.Users
{
    public class UserInfoDto:UserDto
    {
        public string Desc { get; set; }
        public string HomePath { get; set; }
        public string RealName => Name;
        public long UserId => Id;
        public string Username => Account;
        /// <summary>
        /// 角色
        /// </summary>
        public List<string> Role { get; set; }
    }
}
