﻿#pragma warning disable CS8618 
namespace Abc.Application.Users;

public class UserDto
{
    /// <summary>
    /// ID
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 用户名
    /// </summary>
    public string Account { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    public DateTime Birthday { get; set; }

    /// <summary>
    /// 岁数
    /// </summary> 
    public int Age { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public string Mobile { get; set; }
}