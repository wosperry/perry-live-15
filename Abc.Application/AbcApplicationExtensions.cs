﻿using System.Reflection;
using Abc.Application.Users; 
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Abc.Application;

public static class AbcApplicationExtensions
{
    public static IServiceCollection ConfigureApplication(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddTransient<IUserService, UserService>()
            .AddAutoMapper(options => { options.AddMaps(Assembly.GetExecutingAssembly()); });
        return services;
    }
}