﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abc.Application.Dtos
{
    public class PagedResultDto<TData>
    {
        public int Total { get; set; }
        public List<TData> Items { get; set; } = new();

        public PagedResultDto()
        {

        }
        public PagedResultDto(int total, List<TData> items)
        {
            Total = total;
            Items = items;
        } 
    }
}
