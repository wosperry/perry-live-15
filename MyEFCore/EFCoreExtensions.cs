﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Reflection;

namespace MyEFCore
{
    public static class EFCoreExtensions
    {
        public static IServiceCollection AddMyDbContext<TDbContext>(this IServiceCollection services, Action<DbContextOptionsBuilder, string> optionAction)
            where TDbContext : BaseDbContext
        {
            return services
                .AddTransient<SoftDeleteInterceptor, SoftDeleteInterceptor>()
                .AddDbContext<TDbContext>((provider, options) =>
            {
                var configuration = provider.GetService<IConfiguration>();
                var connectionStringName = typeof(TDbContext)
                    .GetCustomAttribute<ConnectionStringNameAttribute>()
                    ?.ConnectionStringName ?? "Default";

                var connectionString = configuration.GetConnectionString(connectionStringName);

                if (string.IsNullOrWhiteSpace(connectionString))
                {
                    throw new ConnectionStringNotSetException($"{typeof(DbContext).Name}应设置 ConnectionStringNameAttribute，并提供与appsettings.json 内配置的连接字符串相匹配的Key，若没有设置，则默认取 Connectionstrings.Default");
                }

                options.AddInterceptors(provider.GetService<SoftDeleteInterceptor>()!);

                optionAction?.Invoke(options, connectionString); 
            });
        }
    }

}
