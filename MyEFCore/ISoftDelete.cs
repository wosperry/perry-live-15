﻿namespace MyEFCore
{
    public interface ISoftDelete
    {
        public bool IsDeleted { get; set; }
    }
}
