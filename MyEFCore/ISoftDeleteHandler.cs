﻿using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace MyEFCore
{
    public interface ISoftDeleteHandler
    {
        public void SoftDeleteExecuting<TEntity>(TEntity entity) where TEntity:class;
    }

}
