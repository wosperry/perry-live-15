﻿namespace MyEFCore
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ConnectionStringNameAttribute : Attribute
    {
        public ConnectionStringNameAttribute(string connectionStringName)
        {
            ConnectionStringName = connectionStringName;
        }

        public string ConnectionStringName { get; }
    }

}
