﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestBaseDbContext
{
    public class StudentEntityTypeConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable("student");

            builder.Property(x => x.Name).HasMaxLength(50).HasColumnName("name").HasComment("名字");
            builder.Property(x => x.NickName).HasMaxLength(50).HasColumnName("nickname").HasComment("昵称");
            builder.Property(x => x.Birthday).HasColumnName("birth").HasComment("生日");
        }
    }
}