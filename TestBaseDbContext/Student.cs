﻿using MyEFCore;

namespace TestBaseDbContext
{
    public class Student : Entity<long>, ISoftDelete
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public DateTime Birthday { get; set; }
        public bool IsDeleted { get; set; }
    }
}