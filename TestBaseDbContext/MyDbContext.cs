﻿using Microsoft.EntityFrameworkCore;
using MyEFCore;

namespace TestBaseDbContext
{
    [ConnectionStringName("Test")]
    public class MyDbContext : BaseDbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }
}