using System.Diagnostics;
using System.Reflection;
using Abc.Application;
using Abc.EFCore;
using JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using Yitter.IdGenerator;

var builder = WebApplication.CreateBuilder(args);
 
// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("aaa", new()
        {
            Title = "Perry 直播写着玩", Version = "v1", Description = "点赞关注"
        });

        var docPath = Path.Combine(AppContext.BaseDirectory, Assembly.GetExecutingAssembly().GetName().Name!+".xml");
        c.IncludeXmlComments(docPath,true);
        
        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            Description = "JWT Authorization header using the Bearer scheme.",
            Name = "Authorization",
            In = ParameterLocation.Header,
            Scheme = "bearer",
            Type = SecuritySchemeType.Http,
            BearerFormat = "JWT"
        });

        c.AddSecurityRequirement(new OpenApiSecurityRequirement {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
            },
            new List<string>()
        } });
    })
    .AddDbContext<DefaultDbContext>(optionsBuilder =>
    {
        optionsBuilder.UseSqlite(builder.Configuration.GetConnectionString("Sqlite"));
    })
    .ConfigureApplication(builder.Configuration)
    .AddJwtBearer(builder.Configuration)
    .AddSnowflake(builder.Configuration);

var seed = builder.Configuration.GetSection("IdGeneratorSeed").Get<ushort>();
YitIdHelper.SetIdGenerator(new IdGeneratorOptions(seed));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/aaa/swagger.json", "系统");
        c.DocExpansion(DocExpansion.List);
    });
}

app.UseHttpsRedirection();
app.UserJwtBearer();
app.MapControllers();

app.Run(); 