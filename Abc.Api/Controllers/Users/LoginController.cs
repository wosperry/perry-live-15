﻿using Abc.Application.Users;
using JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Abc.Api.Controllers.Users;

/// <summary>
/// 登录
/// </summary>
[Route("login")]
public class LoginController : ControllerBase
{
    private readonly IUserService _userService;
    private readonly IJwtService _jwtService;

    /// <summary>
    /// 登录
    /// </summary>
    /// <param name="userService"></param>
    /// <param name="jwtService"></param>
    public LoginController(IUserService userService, IJwtService jwtService)
    {
        _userService = userService;
        _jwtService = jwtService;
    }

    /// <summary>
    /// 登录
    /// </summary>
    [HttpPost]
    [AllowAnonymous]
    public async Task<ActionResult<LoginResultDto>> LoginAsync([FromBody] LoginParameter input)
    {
        if (await _userService.CheckLoginAsync(input.Username, input.Password))
        {
            return Ok(new LoginResultDto() { Token = await _jwtService.GenerateTokenAsync(input.Username) });
        }
        else
        {
            return BadRequest();
        }
    }
}