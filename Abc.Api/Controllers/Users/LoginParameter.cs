﻿// ReSharper disable All
#pragma warning disable CS8618
namespace Abc.Api.Controllers.Users;

/// <summary>
/// 登录参数
/// </summary>
public class LoginParameter
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string Username { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    public string Password { get; set; }
}