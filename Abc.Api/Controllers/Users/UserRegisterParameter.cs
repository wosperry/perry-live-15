﻿// ReSharper disable All
#pragma warning disable CS8618
namespace Abc.Api.Controllers.Users;

/// <summary>
/// 用户注册参数
/// </summary>
public class UserRegisterParameter
{
    /// <summary>
    /// 账号
    /// </summary>
    public string Account { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    public string Password { get; set; }
    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// 手机号码
    /// </summary>
    public string Mobile { get; set; }
    /// <summary>
    /// 生日
    /// </summary>
    public DateTime Birthday { get; set; }
}