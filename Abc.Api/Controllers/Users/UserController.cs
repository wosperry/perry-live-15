﻿using Abc.Application.Dtos;
using Abc.Application.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Abc.Api.Controllers.Users;

/// <summary>
/// 用户管理
/// </summary>
[Authorize]
[Route("user")]
public class UserController:ControllerBase
{
    private readonly IUserService _userService;
    private readonly IHttpContextAccessor _accessor;

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="userService">用户服务</param>
    /// <param name="accessor"></param>
    public UserController(IUserService userService,IHttpContextAccessor accessor)
    {
        _userService = userService;
        _accessor = accessor;
    }

    /// <summary>
    /// 用户注册
    /// </summary>
    /// <param name="input">用户注册参数</param>
    /// <returns>用户DTO</returns>
    [HttpPost]
    [AllowAnonymous]
    public Task<UserDto> RegisterAsync(UserRegisterParameter input) =>
        _userService.RegisterAsync(input.Account, input.Password, input.Name, input.Mobile,input.Birthday);

    /// <summary>
    /// 列表查询
    /// </summary>
    /// <param name="input">分页查询参数</param>
    /// <returns>查询结果</returns>
    [HttpGet]
    public  Task<PagedResultDto<UserDto>> GetPageListAsync(UserPageListQueryParameter input) =>
     _userService.GetPagedListAsync(input.Account, input.Name, input.Mobile, input.Birthday, input.PageIndex,
            input.PageSize);


    /// <summary>
    /// 获取用户信息
    /// </summary>
    [HttpGet("info")]
    public async Task<ActionResult<UserInfoDto>> GetUserInfoAsync()
    {
        var username = _accessor?.HttpContext?.User?.Identity?.Name;
        if (string.IsNullOrWhiteSpace(username))
        {
            return Unauthorized();
        }
        
        var userInfo = await _userService.GetUserInfo(username!);
        return Ok(userInfo);
    } 
}