﻿// ReSharper disable All 
#pragma warning disable CS8618
namespace Abc.Api.Controllers.Users;

/// <summary>
/// 用户分页查询参数
/// </summary>
public class UserPageListQueryParameter
{
    /// <summary>
    /// 账号
    /// </summary>
    public string? Account { get; set; }
    /// <summary>
    /// 姓名
    /// </summary>
    public string? Name { get; set; }
    /// <summary>
    /// 手机号码
    /// </summary>
    public string? Mobile { get; set; }
    /// <summary>
    /// 生日
    /// </summary>
    public DateTime? Birthday { get; set; }
    /// <summary>
    /// 页码
    /// </summary>
    public int PageIndex { get; set; } = 1;
    /// <summary>
    /// 每页数量
    /// </summary>
    public int PageSize { get; set; } = 10;
}