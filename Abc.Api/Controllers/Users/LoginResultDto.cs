﻿// ReSharper disable All
#pragma warning disable CS8618
namespace Abc.Api.Controllers.Users;

/// <summary>
/// 登录结果
/// </summary>
public class LoginResultDto
{
    /// <summary>
    /// Token
    /// </summary>
    public string Token { get; set; }
}