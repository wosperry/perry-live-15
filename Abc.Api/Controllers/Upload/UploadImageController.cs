﻿using System.Net.Http.Headers;
using Abc.Application.Users;
using Microsoft.AspNetCore.Mvc;

namespace Abc.Api.Controllers.Upload;

[Route("upload/img")]
public class UploadImageController : ControllerBase
{
    [HttpPost]
    public async Task<IActionResult> UploadImageAsync(IFormFile file)
    {
        HttpClient client = new HttpClient()
        {
            BaseAddress = new Uri("https://wosperry.com/lsky/api/v1/")
        };
        var response = await client.PostAsync("tokens", JsonContent.Create(new
        {
            email = "abc@wosperry.com",
            password = "Aa123456."
        }));

        response.EnsureSuccessStatusCode();
        var loginResult = await response.Content.ReadFromJsonAsync<LskyLoginResponse>();

        if (!string.IsNullOrWhiteSpace(loginResult?.Data?.Token))
        { 
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {loginResult.Data.Token}");
            
   
 
            var content = new MultipartFormDataContent();
            
            content.Add(new StringContent("3"), "strategy_id");
            var stream = file.OpenReadStream();
            content.Add(new StreamContent(stream), "file"); 

            var uploadResponse = await client.PostAsync("upload", content);
            uploadResponse.EnsureSuccessStatusCode();
            var aaa = await  uploadResponse.Content.ReadAsStringAsync();
        }

        return Ok("");
    }
}

public class LskyLoginResponse
{
    public bool Status { get; set; }
    public string Message { get; set; }
    public LskyLoginResult Data { get; set; }
}

public class LskyLoginResult
{
    public string Token { get; set; }
}