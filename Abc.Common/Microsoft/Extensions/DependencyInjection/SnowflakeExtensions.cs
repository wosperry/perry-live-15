﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Yitter.IdGenerator;

namespace Microsoft.Extensions.DependencyInjection;

public static class SnowflakeExtensions
{
    public static IServiceCollection AddSnowflake(this IServiceCollection services,IConfiguration configuration)
    {
        var seed = configuration.GetSection("IdGeneratorSeed").Get<ushort>();
        YitIdHelper.SetIdGenerator(new IdGeneratorOptions(seed));
        return services;
    }
}