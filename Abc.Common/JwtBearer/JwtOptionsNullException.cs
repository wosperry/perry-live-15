﻿namespace JwtBearer;

public class JwtOptionsNullException : Exception
{
    public JwtOptionsNullException():base("未在配置文件指定 JwtBearerOptions")
    {
        
    }
}