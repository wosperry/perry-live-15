﻿namespace JwtBearer;

public interface IJwtService
{
    public Task<string> GenerateTokenAsync(string user);
}