﻿using System.Security.Cryptography;
using System.Text;

namespace MD5Encrypt;

public class Md5Encryptor
{
    private static readonly MD5 Md5 = MD5.Create();

    public static string Encrypt(string input) =>
        BitConverter.ToString(Md5.ComputeHash(Encoding.Default.GetBytes(input)), 4, 8).Replace("-", "");
}